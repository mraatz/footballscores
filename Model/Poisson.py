# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 16:18:16 2018

@author: Micha
"""

for name in dir():
    if not name.startswith('_'):
        del globals()[name]

import os
#os.chdir('P:\Git-Repo\phd\MicroLoop\Model')
os.chdir('D:\Archiv\\footballscores\Model')

import numpy as np
import matplotlib.pyplot as plt


# score should follow a Poisson distribution

s1 = np.random.poisson(1.5, 64)
s2 = np.random.poisson(1.5, 64)


#n, bins, patches  = plt.hist(s1, bins=12, density=None,normed=None)

plt.figure(num=1,)
plt.clf()
n, bins, patches = plt.hist([s1,s2,], [-0.5,0.5,1.5,2.5,3.5,4.5,5.5,6.5], density=True)
plt.xlabel('Goals')
plt.ylabel('Probability')
plt.title('Histogram of expected goals per team')
plt.text(3, .3, r'$\lambda=1.5 \ N=64$')
plt.grid(True)
plt.savefig('../Figures/Goals.pdf',format='pdf')

plt.figure(num=2,)
plt.clf()
plt.hist(s1-s2, [-6.5,-5.5,-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5,5.5,6.5], density=True)
plt.xlabel('Goal difference')
plt.ylabel('Probability')
plt.title('Histogram of expected goal difference')
plt.text(3, .225, r'$\lambda=1.5 \ N=64$')
plt.grid(True)
plt.savefig('../Figures/GoalDifference.pdf',format='pdf')

np.savetxt('../Data/s1.txt',s1)
np.savetxt('../Data/s2.txt',s2)

